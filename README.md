# PHOG descriptor - feature vector
This is an implementation of the original Phog descriptor that was written in Maltab therefore this code was converted from Matlab to Python. The main difference is that it uses a scikit canny implementation and no ROI (the region of interest is a whole image). The class computes a PHOG descriptor of an image.
The original Matlab implementation is based here: http://www.robots.ox.ac.uk/~vgg/research/caltech/phog.html.

## About PHOG
The PHOG descriptor consists of a histogram of orientation gradients over each image subregion at each resolution level - a Pyramid of Histograms of Orientation Gradients (PHOG). The distance between two PHOG image descriptors then reflects the extent to which the images contain similar shapes and correspond in their spatial layout.
      
## Dependencies

* Scikit (-image)
* Numpy
* Scipy
* OpenCV
* Cython

## Convert matlab code to python

1. Download PyClips http://sourceforge.net/projects/pyclips/files/pyclips/pyclips-1.0/pyclips-1.0.7.348.tar.gz/download
2. run ./setup.py install in cd
3. Download Libermate
    1. cd /path/to/libermate-0.4
    2. ./libermate.py /path/to/matlabfile.m